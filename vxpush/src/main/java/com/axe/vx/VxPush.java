package com.axe.vx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Title VxPush
 * @Package com.axe.vx
 * @Description
 * @Author Axe
 * @Date 2022-08-30 09:32
 */
@SpringBootApplication
public class VxPush {

    public static void main(String[] args) {
        SpringApplication.run(VxPush.class,args);
    }
}
