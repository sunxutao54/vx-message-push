package com.axe.vx.push.common.service.impl;

import com.alibaba.fastjson.JSON;
import com.axe.vx.push.common.pojo.VxUser;
import com.axe.vx.push.common.service.VxPushService;
import com.axe.vx.push.common.utils.DateUtil;
import com.github.kevinsawicki.http.HttpRequest;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Title VxPushServiceImpl
 * @Package com.axe.vx.push.common.service.impl
 * @Description
 * @Author Axe
 * @Date 2022-08-30 09:53
 */
@Service
@PropertySource("classpath:config.properties")
public class VxPushServiceImpl implements VxPushService {

    @Autowired
    private VxUser vxUser;
    @Value("${appId}")
    private String appId;
    @Value("${appSecret}")
    private String appSecret;
    @Value("${templateId}")
    private String templateId;
    @Value("${writer}")
    private String writer;

    /**
    * @Description: 微信公众号消息推送
    * @Author: Axe
    * @Date: 2022/8/30 10:18
    * @Param: []
    * @Return: void
    */
    @Override
    public void push() throws ParseException {
        // 配置
        WxMpInMemoryConfigStorage wxStorage = new WxMpInMemoryConfigStorage();
        wxStorage.setAppId(appId);
        wxStorage.setSecret(appSecret);

        WxMpServiceImpl wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(wxStorage);
        // 获取关注用户openId列表
        List<String> allUserOpenId = this.getWxAllUserOpenId();


        List<WxMpTemplateData> mpTemplateDataArrayList = new ArrayList<>();
        mpTemplateDataArrayList.add(new WxMpTemplateData("data",new SimpleDateFormat("yyyy年MM月dd日").format(new Date()),"#00FF00"));
        mpTemplateDataArrayList.add(new WxMpTemplateData("city", vxUser.getCity(),"#6698FF"));
        mpTemplateDataArrayList.add(new WxMpTemplateData("birthday",vxUser.getBirthday(),"#C48189"));
        mpTemplateDataArrayList.add(new WxMpTemplateData("love_day", String.valueOf(DateUtil.difference(vxUser.getLoveDay(),"yyyy-MM-dd")),"#E8ADAA"));
        mpTemplateDataArrayList.add(new WxMpTemplateData("birthday_day", String.valueOf(DateUtil.nextDateDays(1,19)),"#E38AAE"));
        mpTemplateDataArrayList.add(new WxMpTemplateData("name", vxUser.getName(),"#FAAFBE"));
        mpTemplateDataArrayList.add(new WxMpTemplateData("how_today", String.valueOf(DateUtil.difference(vxUser.getBirthday(),"yyyy-MM-dd")),"#F75D59"));
        mpTemplateDataArrayList.add(new WxMpTemplateData("else",writer,"#F6358A"));

        allUserOpenId.forEach(openId -> {
            WxMpTemplateMessage message = WxMpTemplateMessage.builder().toUser(openId).templateId(templateId).data(mpTemplateDataArrayList).build();
            try {
                wxMpService.getTemplateMsgService().sendTemplateMsg(message);
            } catch (WxErrorException e) {
                throw new RuntimeException(e);
            }
        });
    }

    /**
    * @Description: 获取关注用户的openId
    * @Author: Axe
    * @Date: 2022/8/30 10:16
    * @Param: []
    * @Return: java.util.List<java.lang.String>
    */
    @Override
    public List<String> getWxAllUserOpenId() {
        String accessToken = getAccessToken();
        String userUrl = "https://api.weixin.qq.com/cgi-bin/user/get?access_token="+accessToken;
        String allUserBody = HttpRequest.get(userUrl).body();
        Map<String,Object> getUserResult = JSON.parseObject(allUserBody);
        Map<String, Object> result = (Map<String, Object>) getUserResult.get("data");
        List<String> list = new ArrayList<>();
        if (!"0".equals(getUserResult.get("count"))){
            list =(List<String>) result.get("openid");
        }
        return list;
    }


    public String getAccessToken(){
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" +
                appId + "&secret=" + appSecret;
        String body = HttpRequest.get(url).body();
        return JSON.parseObject(body).get("access_token").toString();
    }
}
