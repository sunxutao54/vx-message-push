package com.axe.vx.push.common.utils;

import java.util.Arrays;
import java.util.List;

/**
 * @Title StringUtils
 * @Package com.axe.utils
 * @Description
 * @Author Axe
 * @Date 2022-08-29 15:13
 */
public class StringUtils {

    /**
    * @Description: 判断参数字符串是否为空白，如果为空白返回true，否则返回false
    * @Author: Axe
    * @Date: 2022/8/29 15:15
    * @Param: [param]
    * @Return: boolean
    */
    public static boolean isNotBlank(String param){
        return param != null && param.trim().length() > 0;
    }

    /**
     * @Description: 判断参数字符串是否为空白，如果为空白返回true，否则返回false
     * @Author: Axe
     * @Date: 2022/8/29 15:15
     * @Param: [param]
     * @Return: boolean
     */
    public static boolean isNotBlank(String ... params){
        boolean flag = false;
        for (String str : params) {
            flag = isNotBlank(str);
        }
        return flag;
    }

    /**
    * @Description: 拼接字符串
    * @Author: Axe
    * @Date: 2022/8/29 15:28
    * @Param: [args]
    * @Return: java.lang.String
    */
    public static String joint(String ... args){
        StringBuilder builder = new StringBuilder();
        for (String str : args){
            builder.append(str);
        }
        return builder.toString();
    }

    /**
    * @Description: 将字符串集合拼接为一条字符串，使用规定分割符分割
    * @Author: Axe
    * @Date: 2022/8/29 15:32
    * @Param: [stringList, delimiter]
    * @Return: java.lang.String
    */
    public static String joint(List<String> stringList , String delimiter){
        StringBuilder builder = new StringBuilder();
        for (String str : stringList) {
            builder.append(str).append(delimiter);
        }
        builder.setLength(builder.length() - 1);
        return builder.toString();
    }

    /**
    * @Description: 分割字符串
    * @Author: Axe
    * @Date: 2022/8/29 16:04
    * @Param: [delimiter]
    * @Return: java.util.List<java.lang.String>
    */
    public static List<String> split(String string,String delimiter){
        return Arrays.asList(string.split(delimiter));
    }
}
