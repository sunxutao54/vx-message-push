package com.axe.vx.push.controller;

import com.axe.vx.push.common.service.VxPushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.List;

/**
 * @Title VxPushController
 * @Package com.axe.vx.push.controller
 * @Description
 * @Author Axe
 * @Date 2022-08-30 09:51
 */
@RestController
@RequestMapping("/vx")
public class VxPushController {

    @Autowired
    private VxPushService service;

    @GetMapping("/push")
    public void push() throws ParseException {
        service.push();
    }

    @GetMapping("/list")
    public List<String> list(){
        return service.getWxAllUserOpenId();
    }

}
