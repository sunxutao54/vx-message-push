package com.axe.vx.push.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Title DateUtil
 * @Package com.axe.utils
 * @Description
 * @Author Axe
 * @Date 2022-08-29 09:50
 */
public class DateUtil {

    private static final String  DATE_ALL_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static  SimpleDateFormat sf = new SimpleDateFormat(DATE_ALL_FORMAT);

    /**
     * @Description: 通过long类型的时间戳转为日期格式
     * 日期格式默认为：yyyy-MM-dd HH:mm:ss
     * @Author: Axe
     * @Date: 2022/8/29 09:55
     * @Return: java.lang.String
     */
    public static String getDate(){
        return sf.format(new Date(System.currentTimeMillis()));
    }

   /**
   * @Description: 通过long类型的时间戳转为日期格式
   * @Author: Axe
   * @Date: 2022/8/29 10:56
   * @Param: [time 时间戳（毫秒）, dateFormat 日期格式]
   * @Return: java.lang.String
   */
    public static String getDate(Long time,String dateFormat){
        if (dateFormat != null && dateFormat.length() > 0){
            sf = new SimpleDateFormat(dateFormat);
        }
        if (Objects.isNull(time)){
            time = System.currentTimeMillis();
        }
        return sf.format(new Date(time));
    }

    /**
     * @Description: 通过long类型的时间戳转为日期格式
     * @Author: Axe
     * @Date: 2022/8/29 09:55
     * @Param: [dateFormat 日期格式]
     * @Return: java.lang.String
     */
    public static String getDate(String dateFormat){
        if (dateFormat != null && dateFormat.length() > 0){
            sf = new SimpleDateFormat(dateFormat);
        }
        long time = System.currentTimeMillis();
        return sf.format(new Date(time));
    }

    /**
     * @Description: 通过long类型的时间戳转为日期格式
     * @Author: Axe
     * @Date: 2022/8/29 09:55
     * @Param: [time 时间戳（毫秒）]
     * @Return: java.lang.String
     */
    public static String getDate(long time){
        return sf.format(new Date(time));
    }


    /**
    * @Description: 根据日期格式字符串转为时间戳
    * @Author: Axe
    * @Date: 2022/8/29 10:58
    * @Param: [date 日期字符串, dateFormat 日期格式]
    * @Return: long
    */
    public static long getTimeByDate(String date,String dateFormat) throws ParseException {
        if (dateFormat != null && dateFormat.length() > 0){
            sf = new SimpleDateFormat(dateFormat);
        }
        return sf.parse(date).getTime();
    }


    /**
    * @Description: 计算两天日期相差几天;兼容参数大小
    * @Author: Axe
    * @Date: 2022/8/29 10:12
    * @Param: [time1 时间戳（毫秒）, time2 时间戳（毫秒）]
    * @Return: long
    */
    public static long difference(long time1,long time2){
        long difference = 0;
        if (time2 > time1){
            difference = time2 - time1;
        }else if (time1 > time2){
            difference = time1 - time2;
        }
        return difference / 1000 / 60 / 60 / 24;
    }

    /**
    * @Description: 计算两天日期相差几天;兼容参数大小
    * @Author: Axe
    * @Date: 2022/8/29 10:32
    * @Param: [date1 日期1, date2 日期1, dateFormat 日期格式]
    * @Return: long
    */
    public static long difference(String date1,String date2,String dateFormat) throws ParseException {
        long time1 = getTimeByDate(date1,dateFormat);
        long time2 = getTimeByDate(date2,dateFormat);
        return difference(time1,time2);
    }

    /**
     * @Description: 计算两天日期相差几天;兼容参数大小
     * @Author: Axe
     * @Date: 2022/8/29 10:32
     * @Param: [date1 日期1, date2 日期1, dateFormat 日期格式]
     * @Return: long
     */
    public static long difference(String date1,String dateFormat) throws ParseException {
        long time1 = getTimeByDate(date1,dateFormat);
        long time2 = System.currentTimeMillis();
        return difference(time1,time2);
    }

    /**
    * @Description: 计算两天日期相差几天;兼容参数大小
    * @Author: Axe
    * @Date: 2022/8/29 10:31
    * @Param: [date1, date2]
    * @Return: long
    */
    public static long difference(Date date1,Date date2){
        long time1 = date1.getTime();
        long time2 = date2.getTime();
        return difference(time1,time2);
    }

    /**
    * @Description: 获取两个时间段中的所有日期；默认包含开始日期和结束日期
    * @Author: Axe
    * @Date: 2022/8/29 11:02
    * @Param: [startDate 开始日期, endDate 结束日期]
    * @Return: java.lang.String[]
    */
    public static List<String> getDateList(String startDate, String endDate){
        List<String> result = new ArrayList<>();
        sf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date start_date = sf.parse(startDate);
            Date end_date = sf.parse(endDate);
            Calendar tempStart = Calendar.getInstance();
            tempStart.setTime(start_date);
            Calendar tempEnd = Calendar.getInstance();
            tempEnd.setTime(end_date);

            while (tempStart.before(tempEnd)||tempStart.equals(tempEnd)) {
                result.add(sf.format(tempStart.getTime()));
                tempStart.add(Calendar.DAY_OF_YEAR, 1);
            }
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    /**
    * @Description: 获取两个时间段中的所有日期；默认包含开始日期和结束日期
    * @Author: Axe
    * @Date: 2022/8/29 11:18
    * @Param: [startDate, endDate, contain 是否包含开始日期和结束日期]
    * @Return: java.util.List<java.lang.String>
    */
    public static List<String> getDateList(String startDate, String endDate,boolean contain){
        if (contain){
            return getDateList(startDate,endDate);
        }
        List<String> result = new ArrayList<>();
        sf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date start_date = sf.parse(startDate);
            Date end_date = sf.parse(endDate);
            Calendar tempStart = Calendar.getInstance();
            tempStart.setTime(start_date);
            Calendar tempEnd = Calendar.getInstance();
            tempEnd.setTime(end_date);
            while (tempStart.before(tempEnd)||tempStart.equals(tempEnd)) {
                String format = sf.format(tempStart.getTime());
                if (!format.equals(startDate) && !format.equals(endDate)){
                    result.add(format);
                }
                tempStart.add(Calendar.DAY_OF_YEAR, 1);
            }
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    /**
    * @Description: 计算当前日期距离今某月某日还有多少天（例如：计算生日）
    * @Author: Axe
    * @Date: 2022/8/29 14:52
    * @Param: [month, day]
    * @Return: int
    */
    public static long nextDateDays(int month,int dayOfMonth){
        Calendar calendar = Calendar.getInstance();
        // 当前年
        int year = calendar.get(Calendar.YEAR);
        int newMonth = calendar.get(Calendar.MONTH) + 1;
        if (newMonth > month){
            year = year + 1;
        }
        month = month - 1;
        calendar.set(year,month,dayOfMonth);
        return difference(calendar.getTimeInMillis(),System.currentTimeMillis());
    }

    /**
    * @Description: 计算某个日期多少天后的日期
    * @Author: Axe
    * @Date: 2022/8/29 16:14
    * @Param: [date, days]
    * @Return: java.lang.String
    */
    public static String add(String date,int days) throws ParseException {
        sf = new SimpleDateFormat("yyyy-MM-dd");
        Date param = sf.parse(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(param);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int newDay = day+days;
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month);
        calendar.set(Calendar.DAY_OF_MONTH,newDay);
        return sf.format(calendar.getTime());
    }
}
