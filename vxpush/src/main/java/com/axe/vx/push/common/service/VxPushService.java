package com.axe.vx.push.common.service;

import java.text.ParseException;
import java.util.List;

/**
 * @Title VxPushService
 * @Package com.axe.vx.push.common.service.impl
 * @Description
 * @Author Axe
 * @Date 2022-08-30 09:52
 */
public interface VxPushService {

    /**
    * @Description: 推送消息
    * @Author: Axe
    * @Date: 2022/8/30 09:54
    * @Param: []
    * @Return: void
    */
    void push() throws ParseException;

    /**
    * @Description: 获取关注用户的openId
    * @Author: Axe
    * @Date: 2022/8/30 10:15
    * @Param: []
    * @Return: java.util.List<java.lang.String>
    */
    List<String> getWxAllUserOpenId();




}
