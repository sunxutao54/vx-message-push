package com.axe.vx.push.common.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @Title VxUser
 * @Package com.axe.vx.push.common.pojo
 * @Description
 * @Author Axe
 * @Date 2022-08-30 09:39
 */
@Component
@PropertySource("classpath:config.properties")
@Data
public class VxUser implements Serializable {

    @ApiModelProperty("推送用户姓名")
    @Value("${name}")
    private String name;

    @ApiModelProperty("所在城市")
    @Value("${city}")
    private String city;

    @ApiModelProperty("推送用户生日")
    @Value("${birthday}")
    private String birthday;

    @ApiModelProperty("恋爱日期")
    @Value("${loveDay}")
    private String loveDay;

    @ApiModelProperty("生日月份")
    @Value("${month}")
    private int month;

    @ApiModelProperty("生日日期")
    @Value("${day}")
    private int day;
}
